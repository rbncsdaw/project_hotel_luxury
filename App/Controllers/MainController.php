<?php
/**
 * Main controller for application. 
 * Project Hotel Luxury.
 * Rubén Rocha, Andrea Muñoz.
 * 2021
 */
namespace App\Controllers;
use \App\Models\Role as Role;
use \App\Models\User as User;
use \App\Persistence\UserDB as UserDB;
use \App\Persistence\RoleDB as RoleDB;

/**
 * Application main controller.
 */
class MainController {
        
    /**
     * Create a new main controller.
     *
     * @return void
     */
    public function __construct() {
        // Start session.
        session_start();
        // Default user.
        if ( !isset($_SESSION['username']) ) {
            $_SESSION['username'] = USER_EMAIL_ANONYMOUS;
        }
        // Raise if not exists user in database.
        if ( !UserDB::exists($_SESSION['username']) ){
            throw new \Exception('No se encuentra el usuario "'.$_SESSION['username'].'".');
        }
    }
    
    /**
     * Get the current user of the application.
     *
     * @return \App\Models\User User
     */
    public function get_current_user() {
        $email = $_SESSION['username'];
        $user = UserDB::read_by_email($email);        
        return $user;
    }
    
    /**
     * Set the current user of the application.
     *
     * @param \App\Models\User $user User
     * @return void
     */
    public function set_current_user($user) {
        $_SESSION['username'] = $user->email;        
    }
    
    /**
     * Login into the application.
     *
     * @param  string $username Username.
     * @param  string $password Password.
     * @return void
     */
    public function login($username, $password) {
        // Validate parameters: nook -> redirecto to self page.        
        if ( empty($username) || empty($password) ) {
            header('Location:' . php_self() . '?code=5');
            exit();
        }
        // Get password hash from DB.
        $password_hash = UserDB::get_password($username);
        // Validate password.
        $is_valid = password_verify($password, $password_hash);
        if ( $is_valid ){
            session_destroy();
            session_start();
            $_SESSION['username'] = $username;
            header('Location:index.php?code=8');
            exit();
        } else {
            header('Location:' . php_self() . '?code=1');
            exit();
        }
    }
    
    /**
     * Logout into the application.
     *
     * @return void
     */
    public function logout() {        
        session_destroy();
        header('Location:index.php?code=7');
        exit();
    }
    
    /**
     * Register a new user (from register form).
     *
     * @param  string $name Name of the user.
     * @param  string $email Email (application username).
     * @param  string $phone Phone.
     * @param  string $address Adrress.
     * @param  string $password Password.
     * @param  string $password2 Confirmed password.
     * @return void
     */
    public function register($name, $email, $phone, $address, $password, $password2) {
        try {
            // Validate parameters: nook -> redirecto to self page.
            if ( empty($name) || empty($email) || empty($phone) || 
                empty($address) || empty($password) || empty($password2) ) {
                header('Location:' . php_self() . '?code=5');
                exit();
            }
            // Validate password: nook -> redirect to self page.
            if ( !($password === $password2) ) {
                header('Location:' . php_self() . '?code=6');
                exit();
            }
            // Create user into database.
            $password_hash = password_hash($password, PASSWORD_DEFAULT);
            $role = RoleDB::read_by_name(Role::ROLE_REGISTER);
            $new_user = new User(0, $name, $email, $phone, 
                                 $address, $password_hash, $role);
            UserDB::create($new_user);
            // Login with new user.
            $this->login($email, $password);
            // Redirect to home.
            header('Location:index.php?code=110');
            exit();
        } catch (\PDOException $e) {
            // Duplicate user -> redirect to self page.
            if ( $e->getCode()==='23000' ) {
                header('Location:'.php_self().'?code=120&name=' . $name .
                       '&phone=' . $phone . '&address='. $address .
                       '&email='. $email);
                exit();
            } else {
                throw $e;
            }
        } 
    }
    
    /**
     * Create a role by id.
     *
     * @param  string $name Name of the role.
     * @return void
     */
    public function create_role($name) {
        try {
            $role = new Role(0, $name);
            if ( empty($name) ) {            
                header('Location:' . php_self() . '?code=5');
                exit();
            } else {
                RoleDB::create($role);
                header('Location:' . php_self() . '?code=9');
                exit();
            }
            return null;
        } catch (\PDOException $e) {
            if ( $e->getCode()==='23000' ) {
                header('Location:' . php_self() . '?code=9');
                exit();
            } else {
                throw $e;
            }
        }        
    }
    
    /**
     * Read a role by id.
     *
     * @param int $id Identification of the rol.
     * @return \App\Models\Role Role.
     */
    public function read_role($id){
        $role = RoleDB::read($id);
        return $role;
    }
    
    /**
     * Update a role by id.
     *
     * @param  int $id Identification of the role.
     * @param  string $name Name of the role.
     * @return void
     */
    public function update_role($id, $name){
        if ( empty($name) || empty($id) ) {
            header('Location:' . php_self() . '?code=5');
            exit();
        } else {
            $role = new Role($id, $name);
            RoleDB::update($role);
            header('Location:' . php_self() . '?code=11');
            exit();
        }
    }
    
    /**
     * Delete a role by id.
     *
     * @param  int $id Identification of the rol
     * @return void
     */
    public function delete_role($id){        
        if ( empty($id) ) {            
            header('Location:' . php_self() . '?code=5');
            exit();
        } else {
            RoleDB::delete($id);            
            header('Location:' . php_self() . '?code=10');
            exit();
        }
    }
    
    /**
     * Count numbrer of roles.
     *
     * @return int Number of roles.
     */
    public function count_roles(){
        return RoleDB::count();
    }
    
    /**
     * Show details of a role.
     *
     * @param  int $id Identification of the role.
     * @return void
     */
    public function details_role($id){
        if ( empty($id) ) {            
            header('Location:' . php_self() . '?code=5');
            exit();
        } else {
            header('Location:' . php_self() . '?id='.$id);
            exit();
        }
    }
    
    /**
     * Read all roles (pageable).
     *
     * @param  int $count Maximun number of roles to read.
     * @param  int $offset Offset
     * @return mixed Array of roles.
     */
    public function read_all_roles($count=null, $offset=0){
        $roles = RoleDB::read_all($count, $offset);
        return $roles;
    }
    
    /**
     * Get list of roles (for dropdown control).
     *
     * @return mixed Array of tuples (id, name).
     */
    public function get_roles_list(){
        return RoleDB::list();
    }
    
    /**
     * Create a user.
     *
     * @param  string $name Name of the user.
     * @param  string $email Email.
     * @param  string $phone Phone.
     * @param  string $address Address.
     * @param  int $role_id Identetification of the rol.
     * @return \App\Models\User User.
     */
    public function create_user($name, $email, $phone, $address, $role_id){
        try {            
            if ( empty($name) || empty($email) || empty($phone) || 
                 empty($address) || empty($role_id) ) {            
                header('Location:' . php_self() . '?code=5');
                exit();
            } else {                
                $password_hash = password_hash(DEFAULT_PASSWORD, PASSWORD_DEFAULT);
                $role = RoleDB::read($role_id);
                $user = new User(0, $name, $email, $phone, 
                                 $address, $password_hash, $role);
                UserDB::create($user);
                header('Location:' . php_self() . '?code=9');
                exit();
            }
            return null;
        } catch (\PDOException $e) {
            if ( $e->getCode()==='23000' ) {
                header('Location:' . php_self() . '?code=9');
                exit();
            } else {
                throw $e;
            }
        }   
    }

        
    /**
     * Read a user by id.
     *
     * @param  int $id Identificator of the user.
     * @return \App\Models\User User. 
     */
    public function read_user($id){        
        $user = UserDB::read($id);
        return $user;
    }
    
    /**
     * Update a user by id.
     *
     * @param  int $id Identification of the user.
     * @param  string $name Name of the user.
     * @param  string $email Email.
     * @param  string $phone Phone number.
     * @param  string $address Address.
     * @param  string $password_hash Password hash.
     * @param  int $role_id Identificacion of the role.
     * @return void
     */
    public function update_user($id, $name, $email, $phone, $address, $password_hash, $role_id){
        if ( empty($name) || empty($email) || empty($phone) || 
             empty($address) || empty($password_hash) || empty($role_id) ) {            
            header('Location:' . php_self() . '?code=5');
            exit();
        } else {
            $role = RoleDB::read($role_id);
            $user = new User($id, $name, $email, $phone, 
                                $address, $password_hash, $role);
            UserDB::update($user);
            header('Location:' . php_self() . '?code=9');
            exit();
        }
    }
    
    /**
     * Delete a user by id.
     *
     * @param  int $id Identificacion of the user.
     * @return void
     */
    public function delete_user($id){        
        if ( empty($id) ) {            
            header('Location:' . php_self() .'?code=5');
        } else {
            UserDB::delete($id);
            header('Location:' . php_self() . '?code=10');
        }
    }
    
    /**
     * Count number of users.
     *
     * @return int Number of users.
     */
    public function count_users(){
        return UserDB::count();
    }
    
        
    /**
     * Show user details.
     *
     * @param  int $id Identification of the user.
     * @return void
     */
    public function details_user($id){        
        if ( empty($id) ) {            
            header('Location:' . php_self() . '?code=5');
        } else {
            header('Location:' . php_self() . '?id='.$id);
        }
    }

    /**
     * Read all users (pageable).
     *
     * @param  int $count Maximun number of users to read.
     * @param  int $offset Offset.
     * @return mixed Array of users.
     */
    public function read_all_users($count=null, $offset=0){
        $users = UserDB::read_all($count, $offset);
        return $users;
    }
        
    /**
     * Discard changes.
     *
     * @return void
     */
    public function discard(){
        header('Location: ' . php_self());
        exit();
    }

}

?>