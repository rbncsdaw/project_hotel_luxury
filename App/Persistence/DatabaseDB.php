<?php
/**
 * Persistence database connection.
 * Project Hotel Luxury.
 * Rubén Rocha, Andrea Muñoz.
 * 2021
 */
namespace App\Persistence;

/**
 * Create new database connection.
 */
class DatabaseDB {
    
    use \App\Traits\SetterGetterTrait;
    
    private $pdo;

    /**
     * Create a new database connection.
     *
     * @return void
     */
    public function __construct() {
        $this->pdo = $this->create_pdo();
    }
    
    /**
     * Close the database connection.
     *
     * @return void
     */
    public function __destruct() {
        unset($this->pdo);
    }
    
    /**
     * Create PDO.
     *
     * @return \PDOStatement PDO.
     */
    private function create_pdo() {
        $data = $this->load_config();
        $pdo = new \PDO(...$data);
        $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        return $pdo;
    }
        
    /**
     * Load a database configuration from file.
     *
     * @return mixed Array of connection data.
     * @throws Exception The configuration file is invalid.
     */
    private function load_config() {
        // Read file XML and create DOM.
        $root = new \DOMDocument();
        $root->load(CONFIG_DB_XML);
        $is_valid = $root->schemaValidate(CONFIG_DB_XSD);
        // Load data from file.
        if ( $is_valid ) {
            $xml = simplexml_load_file(CONFIG_DB_XML);
            $host = (string)$xml->xpath("//host")[0];
            $name = (string)$xml->xpath("//name")[0];
            $user = (string)$xml->xpath("//user")[0];
            $password = (string)$xml->xpath("//password")[0];
            $dsn = 'mysql:dbname=' . $name . ';host=' . $host;
            $data = array();
            $data[] = $dsn;
            $data[] = $user;
            $data[] = $password;
        } else {
            throw new \Exception("Revisar configuración de la base de datos.");
        }
        return $data;
    }

}

?>