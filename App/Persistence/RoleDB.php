<?php
/**
 * Persisten for roles.
 * Project Hotel Luxury.
 * Rubén Rocha, Andrea Muñoz.
 * 2021
 */
namespace App\Persistence;
use \App\Persistence\DatabaseDB as DatabaseDB;

/**
 * Persistence of roles class.
 */
class RoleDB {

    const SQL_CREATE = <<<'EOD'
        INSERT INTO `roles` (`nombre_rol`)            
            VALUES (:name);
        EOD;
    const SQL_READ = <<<'EOD'
        SELECT *
            FROM `roles`
            WHERE `id` = :id;
        EOD;
    const SQL_UPDATE = <<<'EOD'
        UPDATE `roles`
            SET `nombre_rol` = :name
            WHERE `id` = :id;
        EOD;
    const SQL_DELETE = <<<'EOD'
        DELETE FROM `roles`
            WHERE `id` = :id;
        EOD;
    const SQL_COUNT = <<<'EOD'
        SELECT COUNT(*)
            FROM `roles`;
        EOD;
    const SQL_READ_BY_NAME = <<<'EOD'
        SELECT *
            FROM `roles`
            WHERE `nombre_rol` = :name;
        EOD;
    const SQL_READ_ALL = <<<'EOD'
        SELECT * 
            FROM `roles`
            ORDER BY `id`;
        EOD;
    const SQL_READ_ALL_PAGE = <<<'EOD'
        SELECT * 
            FROM `roles`
            ORDER BY `id`
            LIMIT :count OFFSET :offset;
        EOD;
    const SQL_LIST = <<<'EOD'
            SELECT `id`, `nombre_rol`
                FROM `roles`
                ORDER BY `id`;
            EOD;
    
    /**
     * Insert a role into table.
     *
     * @param  \App\Models\Role $role Role
     * @return void
     */
    static public function create($role) {
        $db = new DatabaseDB();
        $stmt = $db->pdo->prepare(self::SQL_CREATE);
        $stmt->bindValue(':name', $role->name, \PDO::PARAM_STR);
        $stmt->execute();
        $role->id = $db->pdo->lastInsertId();
        unset($stmt);
        unset($db);
    }
    
    /**
     * Read a role from table.
     *
     * @param  int $id ID.
     * @return \App\Models\Role Role.
     */
    static public function read($id) {        
        $db = new DatabaseDB();
        $stmt = $db->pdo->prepare(self::SQL_READ);
        $stmt->bindValue(':id', $id, \PDO::PARAM_INT);
        $stmt->execute();
        $roles = $stmt->fetchAll(\PDO::FETCH_FUNC, ['\App\Models\Role','fromNativeData']);
        $role = count($roles) === 1 ? $roles[0] : null;
        unset($stmt);
        unset($db);
        return $role;
    }
    
    /**
     * Update a role into table.
     *
     * @param  \App\Models\Role $role Role.
     * @return void
     */
    static public function update($role) {     
        $db = new DatabaseDB();
        $stmt = $db->pdo->prepare(self::SQL_UPDATE);
        $stmt->bindValue(':id', $role->id, \PDO::PARAM_INT);
        $stmt->bindValue(':name', $role->name, \PDO::PARAM_STR);
        $stmt->execute();        
        unset($stmt);
        unset($db);
    }
    
    /**
     * Delete role from tabla.
     *
     * @param  int $id ID.
     * @return void
     */
    static public function delete($id) {     
        $db = new DatabaseDB();
        $stmt = $db->pdo->prepare(self::SQL_DELETE);
        $stmt->bindValue(':id', $id, \PDO::PARAM_INT);
        $stmt->execute();
        unset($stmt);
        unset($db);
        return null;
    }
        
    /**
     * Count number of roles into table.
     *
     * @return int Number of roles.
     */
    static public function count() {
        $db = new DatabaseDB();
        $stmt = $db->pdo->prepare(self::SQL_COUNT);
        $stmt->execute();
        $count = $stmt->fetchColumn(0);
        unset($stmt);
        unset($db);
        return $count;
    }
    
    /**
     * Read all roles of the table (pageable).
     *
     * @param  int $count Maximun number of roles to read.
     * @param  int $offset Offset.
     * @return mixed Array of roles.
     */
    static public function read_all($count=null, $offset=0) {
        $db = new DatabaseDB();
        if (is_null($count)) {
            $stmt = $db->pdo->prepare(self::SQL_READ_ALL);
        } else {
            $stmt = $db->pdo->prepare(self::SQL_READ_ALL_PAGE);
            $stmt->bindValue(':count', $count, \PDO::PARAM_INT);
            $stmt->bindValue(':offset', $offset, \PDO::PARAM_INT);
        }        
        $stmt->execute();
        $roles = $stmt->fetchAll(\PDO::FETCH_FUNC, ['\App\Models\Role','fromNativeData']);        
        unset($stmt);
        unset($db);
        return $roles;
    }
    
    /**
     * Read a role from table by name.
     *
     * @param  string $name Name of the role.
     * @return \App\Models\Role Role.
     */
    static public function read_by_name($name) {
        $db = new DatabaseDB();
        $stmt = $db->pdo->prepare(self::SQL_READ_BY_NAME);
        $stmt->bindValue(':name', $name, \PDO::PARAM_STR);
        $stmt->execute();
        $roles = $stmt->fetchAll(\PDO::FETCH_FUNC, ['\App\Models\Role','fromNativeData']);
        $role = count($roles) === 1 ? $roles[0] : null;
        unset($stmt);
        unset($db);
        return $role;
    }
    
    /**
     * List of roles (for dropdown control).
     *
     * @return mixed Array of tuples (id, name).
     */
    static public function list() {
        $db = new DatabaseDB();
        $stmt = $db->pdo->prepare(self::SQL_LIST);
        $stmt->execute();
        $items = $stmt->fetchAll(\PDO::FETCH_NUM);
        unset($stmt);
        unset($db);
        return $items;
    }

}

?>