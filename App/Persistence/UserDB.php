<?php
/**
 * Persisten for users.
 * Project Hotel Luxury.
 * Rubén Rocha, Andrea Muñoz.
 * 2021
 */
namespace App\Persistence;
use \App\Persistence\DatabaseDB;
use \App\Models\User;

/**
 * Persistence of users class.
 */
class UserDB {
    
    const SQL_CREATE = <<<'EOD'
        INSERT INTO `usuarios` (`nombre`, `email`, `telf`, 
                                `direccion`, `password`, `rol_usuario`)
            VALUES (:name, :email, :phone,
                    :address, :password_hash, :id_role);
    EOD;
    const SQL_READ = <<<'EOD'
        SELECT *
            FROM `usuarios`
            WHERE `id` = :id;
        EOD;
    const SQL_UPDATE = <<<'EOD'
        UPDATE `usuarios`
            SET `nombre` = :name,
                `email` = :email,
                `telf` = :phone,
                `direccion` = :address,
                `password` = :password_hash,
                `rol_usuario` = :role_id
            WHERE `id` = :id;
        EOD;
    const SQL_DELETE = <<<'EOD'
        DELETE FROM `usuarios`
            WHERE `id` = :id;
        EOD;
    const SQL_COUNT = <<<'EOD'
        SELECT COUNT(*)
            FROM `usuarios`;
        EOD;
    const SQL_READ_BY_EMAIL = <<<'EOD'
        SELECT *
            FROM `usuarios`
            WHERE `email` = :email;
        EOD;
    const SQL_READ_ALL = <<<'EOD'
        SELECT * 
            FROM `usuarios`
            ORDER BY `id`;
        EOD;
    const SQL_READ_ALL_PAGE =  <<<'EOD'
        SELECT * 
            FROM `usuarios`
            ORDER BY `id`
            LIMIT :count OFFSET :offset;
        EOD;
    const SQL_DELETE_ALL = <<<'EOD'
        DELETE FROM `usuarios`
            WHERE `id` = :id;
        EOD;
    const SQL_GET_PASSWORD = <<<'EOD'
            SELECT `password`
                FROM `usuarios`
                WHERE `email` = :email;
            EOD;
    const SQL_EXISTS = <<<'EOD'
            SELECT COUNT(*)
                FROM `usuarios`
                WHERE `email` = :email;
            EOD;

    /**
     * Insert a usesr into table.
     *
     * @param  \App\Models\User $user User.
     * @return void
     */
    static public function create($user) {
        $db = new DatabaseDB();
        $stmt = $db->pdo->prepare(self::SQL_CREATE);
        $stmt->bindValue(':name', $user->name, \PDO::PARAM_STR);
        $stmt->bindValue(':email', $user->email, \PDO::PARAM_STR);
        $stmt->bindValue(':phone', $user->phone, \PDO::PARAM_STR);
        $stmt->bindValue(':address', $user->address, \PDO::PARAM_STR);
        $stmt->bindValue(':password_hash', $user->password_hash, \PDO::PARAM_STR);
        $stmt->bindValue(':id_role', $user->role->id, \PDO::PARAM_INT);
        $stmt->execute();        
        $user->id = $db->pdo->lastInsertId();
        unset($stmt);
        unset($db);
    }

    /**
     * Read a user from table.
     *
     * @param  int $id ID.
     * @return \App\Models\User User.
     */
    static public function read($id) {        
        $db = new DatabaseDB();
        $stmt = $db->pdo->prepare(self::SQL_READ);
        $stmt->bindValue(':id', $id, \PDO::PARAM_INT);
        $stmt->execute();
        $users = $stmt->fetchAll(\PDO::FETCH_FUNC, ['\App\Models\User','fromNativeData']);
        $user = count($users) === 1 ? $users[0] : null;
        unset($stmt);
        unset($db);
        return $user;
    }

    /**
     * Update a user into table.
     *
     * @param  \App\Models\User $user User.
     * @return void
     */
    static public function update($user) {     
        $db = new DatabaseDB();
        $stmt = $db->pdo->prepare(self::SQL_UPDATE);
        $stmt->bindValue(':id', $user->id, \PDO::PARAM_INT);
        $stmt->bindValue(':name', $user->name, \PDO::PARAM_STR);
        $stmt->bindValue(':email', $user->email, \PDO::PARAM_STR);
        $stmt->bindValue(':phone', $user->phone, \PDO::PARAM_STR);
        $stmt->bindValue(':address', $user->address, \PDO::PARAM_STR);
        $stmt->bindValue(':password_hash', $user->password_hash, \PDO::PARAM_STR);
        $stmt->bindValue(':role_id', $user->role->id, \PDO::PARAM_INT);
        $stmt->execute();
        unset($stmt);
        unset($db);
    }

    /**
     * Delete a user from tabla by id.
     *
     * @param  int $id ID.
     * @return void
     */
    static public function delete($id) {     
        $db = new DatabaseDB();
        $stmt = $db->pdo->prepare(self::SQL_DELETE);
        $stmt->bindValue(':id', $id, \PDO::PARAM_INT);
        $stmt->execute();
        unset($stmt);
        unset($db);
    }
  
    /**
     * Count number of users into table.
     *
     * @return int Number of users.
     */
    static public function count() {
        $db = new DatabaseDB();
        $stmt = $db->pdo->prepare(self::SQL_COUNT);
        $stmt->execute();
        $count = $stmt->fetchColumn(0);
        unset($stmt);
        unset($db);
        return $count;
    }

    /**
     * Read all users of the table (pageable).
     *
     * @param  int $count Maximun number of users to read.
     * @param  int $offset Offset.
     * @return mixed Array of users.
     */
    static public function read_all($count=null, $offset=0) {
        $db = new DatabaseDB();
        if (is_null($count)) {
            $stmt = $db->pdo->prepare(self::SQL_READ_ALL);
        } else {
            $stmt = $db->pdo->prepare(self::SQL_READ_ALL_PAGE);
            $stmt->bindValue(':count', $count, \PDO::PARAM_INT);
            $stmt->bindValue(':offset', $offset, \PDO::PARAM_INT);
        }        
        $stmt->execute();
        $users = $stmt->fetchAll(\PDO::FETCH_FUNC, ['\App\Models\User','fromNativeData']);
        unset($stmt);
        unset($db);
        return $users;
    }

    /**
     * Read a user from table by email.
     *
     * @param  string $email Email of the user.
     * @return \App\Models\User User.
     */
    static public function read_by_email($email) {
        $db = new DatabaseDB();
        $stmt = $db->pdo->prepare(self::SQL_READ_BY_EMAIL);
        $stmt->bindValue(':email', $email, \PDO::PARAM_STR);
        $stmt->execute();
        $users = $stmt->fetchAll(\PDO::FETCH_FUNC, ['\App\Models\User','fromNativeData']);
        $user = count($users) === 1 ? $users[0] : null;
        unset($stmt);
        unset($db);
        return $user;
    }
    
    /**
     * Get the password hash of a user by email.
     *
     * @param  string $email Email of the user.
     * @return string Password hash.
     */
    static public function get_password($email) {
        $db = new DatabaseDB();
        $stmt = $db->pdo->prepare(self::SQL_GET_PASSWORD);
        $stmt->bindValue(':email', $email, \PDO::PARAM_STR);
        $stmt->execute();
        $password_hash = $stmt->fetchColumn(0);
        unset($stmt);
        unset($db);
        return $password_hash;
    }
    
    /**
     * Exists a user by email.
     *
     * @param  string $email Email of the user.
     * @return bool Exists user.
     */
    static public function exists($email) {
        $db = new DatabaseDB();
        $stmt = $db->pdo->prepare(self::SQL_EXISTS);
        $stmt->bindValue(':email', $email, \PDO::PARAM_STR);
        $stmt->execute();
        $exists = $stmt->fetchColumn(0);
        unset($stmt);
        unset($db);
        return $exists;
    }

}

?>