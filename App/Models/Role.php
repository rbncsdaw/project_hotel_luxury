<?php
/**
 * Model of a user role.
 * Project Hotel Luxury.
 * Rubén Rocha, Andrea Muñoz.
 * 2021
 */
namespace App\Models;


/**
 * A role.
 */
class Role {
    
    use \App\Traits\SetterGetterTrait;
    use \App\Traits\ToStringTrait;

    public const ROLE_ANONIMOUS = 'tipo_1';
    public const ROLE_REGISTER = 'tipo_2';
    public const ROLE_ADMIN = 'tipo_3';    
    
    private $id;
    private $name;
    
    /**
     * Create a new role.
     *
     * @param  int $id Identification of the role.
     * @param  string $name Name of the role.
     * @return void
     */
    public function __construct($id, $name) {
        $this->id = $id;
        $this->name = $name;
    }
        
    /**
     * Create new role from native data. 
     *
     * @param  int $id Identification of the role.
     * @param  mixed $name Name of the role.
     * @return \App\Models\Role Role. 
     */
    public function fromNativeData($id, $name) {
        $role = new self($id, $name);
        return $role;
    }
}

?>