<?php
/**
 * Model of an application user.
 * Project Hotel Luxury.
 * Rubén Rocha, Andrea Muñoz.
 * 2021
 */
namespace App\Models;
use \App\Persistence\RoleDB;
use \App\Models\Role;

/**
 * An application user.
 */
class User {
    
    use \App\Traits\SetterGetterTrait;
    use \App\Traits\ToStringTrait;
    
    private $id;
    private $name;
    private $email;
    private $phone;
    private $address;
    private $password_hash;
    private $role;
    private $role_name;
    
    /**
     * Create new application user.
     *
     * @param  int $id Identification of the user.
     * @param  string $name Name of the user.
     * @param  string $email Emaiil
     * @param  string $phone Phone number.
     * @return void
     */
    public function __construct($id, $name, $email, $phone, 
                                $address, $password_hash, $role) {
        $this->id = $id;
        $this->name = $name;        
        $this->email = $email;        
        $this->phone = $phone;
        $this->address = $address;
        $this->password_hash = $password_hash;
        $this->role = $role;
        $this->role_name = $role->name;
    }
    
    /**
     * Create new user from native data. 
     *
     * @param  int $id Identification of the user.
     * @param  string $name Name of the user.
     * @param  string $email Email.
     * @param  string $phone Phone number.
     * @return \App\Models\User User.
     */
    public function fromNativeData($id, $name, $email, $phone, 
    $address, $password_hash, $id_role) {
        $role = RoleDB::read($id_role);
        $user = new self($id, $name, $email, $phone, 
                         $address, $password_hash, $role);
        return $user;
    }
    
    /**
     * User is authorized to view a page.
     *
     * @param  string $relative_url Relative url of the page.
     * @return bool Is authotized.
     */
    public function is_authorized($relative_url){
        return ( !array_key_exists($relative_url, RESTRICTED_PAGES) ||
                 in_array($this->role->name, RESTRICTED_PAGES[$relative_url]) );
    }
    
    /**
     * Is valid password.
     *
     * @param  string $password Password.
     * @return bool Is valid password.
     */
    public function is_valid_password($password) {
        return (($this->role->name === Role::ROLE_REGISTER ||
                 $this->role->name === Role::ROLE_ADMIN)
                && password_verify($password, $this->password_hash));
    }
    
    /**
     * User is anonymous role.
     *
     * @return void
     */
    public function is_anonymous() {
        return ($this->role->name === Role::ROLE_ANONIMOUS);
    }
    
    /**
     * User is administrator role.
     *
     * @return void
     */
    public function is_admin() {
        return ($this->role->name === Role::ROLE_ADMIN);
    }
    
    /**
     * User is resgister user role.
     *
     * @return void
     */
    public function is_register() {
        return ($this->role->name === Role::ROLE_REGISTER);
    }

}

?>