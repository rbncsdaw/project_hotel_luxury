<?php
/**
 * Traits setter and getter.
 * Project Hotel Luxury.
 * Rubén Rocha, Andrea Muñoz.
 * 2021
 */
namespace App\Traits;

trait SetterGetterTrait {
        
    public function __get($name) {
        if (property_exists($this, $name)) {
            $value = $this->$name;
        } else {
            throw new \Exception();
        }
        return $value;
    }
    
    public function __set($name, $value) {
        if (property_exists($this, $name)) {
            $this->$name = $value;
        } else {
            throw new \Exception();
        }
        return $value;
    }
    
}


?>