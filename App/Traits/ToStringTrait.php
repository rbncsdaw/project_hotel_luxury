<?php
/**
 * Traits to string.
 * Project Hotel Luxury.
 * Rubén Rocha, Andrea Muñoz.
 * 2021
 */
namespace App\Traits;

trait ToStringTrait {
    
    public function __toString() {
        $s  = '<table>';
        $s .= '    <tr>';
        $s .= '        <td colspan="2"><strong>' . __CLASS__ . ':</strong></td>';
        $s .= '    </tr>';
        foreach (get_object_vars($this) as $key => $value) {
            $s .= '    <tr>';        
            $s .= '        <td>-' . $key . ':</td>';
            $s .= '        <td>' . $value . '</td>';
            $s .= '    </tr>';
        }
        $s .= '</table>';
        return $s;
    }
    
}

?>