<?php
/**
 * Base exception for the application.
 * Project Hotel Luxury.
 * Rubén Rocha, Andrea Muñoz.
 * 2021
 */
namespace App\Core;

/**
 * Base exception for the application.
 */
class AppException extends \Exception {    
            
    /**
     * Create a new application exception.
     *
     * @param  string $message Message.
     * @param  string $title Title.
     * @return void
     */
    public function __construct($message='Algo ha salido mal...', $title='') {
        parent::__construct($message);
        $this->title = $title;
    }

    public function __toString() {
        $s  = '<div class="box-error">';
        $s .= '    <h2>Error</h2>';
        if ( !empty($this->title) ) {
            $s .= '    <h3>' . $this->title . '</h3>';
        }
        $s .= '    <div>' . $this->message . '</div>';        
        $s .= '</div>';
        return $s;
    }
    
}
    
?>