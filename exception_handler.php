<?php
/**
 * Exception handler by default.
 * Project Hotel Luxury.
 * Rubén Rocha, Andrea Muñoz.
 * 2021
 */

set_exception_handler(
      function ($e) {
            include ERROR;
      }
);

?>