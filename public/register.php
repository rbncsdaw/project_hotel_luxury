<?php
/**
 * Register page for project.
 * Project Hotel Luxury.
 * Rubén Rocha, Andrea Muñoz.
 * 2021
 */
require_once '../global.php';
use \App\Controllers\MainController;

$controller = new MainController();
$current_user = $controller->get_current_user();

$code = isset($_REQUEST['code']) ? (int)$_REQUEST['code'] : 0;
$name = isset($_REQUEST['name']) ? $_REQUEST['name'] : 'register_1';
$email = isset($_REQUEST['email']) ? $_REQUEST['email'] : 'register_1@luxury.example';
$phone = isset($_REQUEST['phone']) ? $_REQUEST['phone'] : '900000001';
$address = isset($_REQUEST['address']) ? $_REQUEST['address'] : 'c\ Sin nombre, 1';
$password = isset($_REQUEST['password']) ? $_REQUEST['password'] : 'abc123.';
$password2 = isset($_REQUEST['password2']) ? $_REQUEST['password2'] : 'abc123.';

// Action register.
if ( isset($_REQUEST['action']) && $_REQUEST['action']==='register') {
    $controller->register($name, $email, $phone, $address, $password, $password2);
}

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?php echo SITE_NAME ?></title>
        <!-- Custom CSS -->
        <link rel="stylesheet" href="/css/styles.css"/>
        <link rel="stylesheet" href="/css/tdata.css"/>
        <link rel="stylesheet" href="/css/page_controls.css"/>
    </head>
    <body>
        <div class="container">
            <?php include HEADER ?>
            <?php include NAV ?>
            <main>
                <?php if ( $code ) { ?>
                <div class="box-message">
                    <?php echo T_MESSAGE[$code] ?>
                </div>
                <?php } ?>
                <div class="box-main">
                    <form class="form-main" action="<?php echo php_self() ?>" method="POST">
                        <h2>Register</h2>
                        <label for="name">Nombre</label>
                        <input type="text" name="name" value="<?php echo $name ?>" 
                               placeholder="Introduce tu nombre..."/>
                        <label for="email">Correo</label>
                        <input type="email" name="email"  value="<?php echo $email ?>"
                            placeholder="email@luxury.example"/>
                        <label for="phone">Teléfono</label>
                        <input type="text" name="phone" value="<?php echo $phone ?>" 
                               placeholder="987654321"/>
                        <label for="address">Dirección</label>
                        <input type="text" name="address" value="<?php echo $address ?>"
                               placeholder="Introduce tu dirección..."/>
                        <label for="password">Contraseña</label>
                        <input type="password" name="password" value="<?php echo $password ?>"
                               placeholder="Introduce una contraseña..."/>
                        <label for="password2">Repetir contraseña</label>
                        <input type="password" name="password2" value="<?php echo $password2 ?>"
                               placeholder="Repite la contraseña..."/>
                        <button type="submit" name="action" value="register">Enviar</button>
                    </form>
                </div>
            </main>
            <?php include FOOTER ?>
        </div>
    </body>    
</html>
