<?php
/**
 * User profile page for project.
 * Project Hotel Luxury.
 * Rubén Rocha, Andrea Muñoz.
 * 2021
 */
require_once '../global.php';
use \App\Controllers\MainController;

$controller = new MainController();
$current_user = $controller->get_current_user();

$code = isset($_REQUEST['code']) ? (int)$_REQUEST['code'] : 0;

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?php echo SITE_NAME ?></title>
        <!-- Custom CSS -->
        <link rel="stylesheet" href="/css/styles.css"/>
        <link rel="stylesheet" href="/css/tdata.css"/>
        <link rel="stylesheet" href="/css/page_controls.css"/>
    </head>
    <body>
        <div class="container">
            <?php include HEADER ?>
            <?php include NAV ?>
            <main>
                <?php if ( $code ) { ?>
                <div class="box-message">
                    <?php echo T_MESSAGE[$code] ?>
                </div>
                <?php } ?>
                <div class="box-main">
                    <form class="form-main" action="<?php echo php_self() ?>" method="POST">
                        <h2>Datos usuario</h2>
                        <label for="name">Nombre</label>
                        <input type="text" name="name" value="<?php echo $current_user->name ?>" 
                                placeholder="Introduce tu nombre..."/>
                        <label for="email">Correo</label>
                        <input type="email" name="email"  value="<?php echo $current_user->email ?>"
                            placeholder="email@luxury.example"/>
                        <label for="phone">Teléfono</label>
                        <input type="text" name="phone" value="<?php echo $current_user->phone ?>" 
                                placeholder="987654321"/>
                        <label for="address">Dirección</label>
                        <input type="text" name="address" value="<?php echo $current_user->address ?>"
                                placeholder="Introduce tu dirección..."/>                    
                        <div>
                            <button type="submit" name="action" value="update">Actualizar</button>
                            <button type="button" onclick="location.href='/password.php'">Cambiar contraseña</button>
                        </div>
                    </form>
                </div>
            </main>
            <?php include FOOTER ?>
        </div>
    </body>    
</html>
