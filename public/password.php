<?php
/**
 * User profile page for project.
 * Project Hotel Luxury.
 * Rubén Rocha, Andrea Muñoz.
 * 2021
 */
require_once '../global.php';
use \App\Controllers\MainController;

$controller = new MainController();
$current_user = $controller->get_current_user();

$code = isset($_REQUEST['code']) ? (int)$_REQUEST['code'] : 0;
$password = isset($_REQUEST['password']) ? $_REQUEST['password'] : 'abc123.';
$password2 = isset($_REQUEST['password2']) ? $_REQUEST['password2'] : 'abc123.';

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?php echo SITE_NAME ?></title>
        <!-- Custom CSS -->
        <link rel="stylesheet" href="/css/styles.css"/>
        <link rel="stylesheet" href="/css/tdata.css"/>
        <link rel="stylesheet" href="/css/page_controls.css"/>
    </head>
    <body>
        <div class="container">
            <?php include HEADER ?>
            <?php include NAV ?>
            <main>
                <?php if ( $code ) { ?>
                <div class="box-message">
                    <?php echo T_MESSAGE[$code] ?>
                </div>
                <?php } ?>
                <div class="box-main">
                    <form class="form-main" action="<?php echo php_self() ?>" method="POST">
                        <h2>Contraseña</h2>
                        <label for="password">Contraseña</label>
                        <input type="password" name="password" value="<?php echo $password ?>"
                               placeholder="Introduce una contraseña..."/>
                        <label for="password2">Repetir contraseña</label>
                        <input type="password" name="password2" value="<?php echo $password2 ?>"
                               placeholder="Repite la contraseña..."/>
                        <div>
                            <button type="submit" name="action" value="update_password">Modificar</button>
                            <button type="button" onclick="location.href='/user.php'">Volver</button>
                        </div>
                        
                    </form>
                </div>
            </main>
            <?php include FOOTER ?>
        </div>
    </body>    
</html>
