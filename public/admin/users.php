<?php
/**
 * Administration zone: user management page.
 * Project Hotel Luxury.
 * Rubén Rocha, Andrea Muñoz.
 * 2021
 */
require_once '../../global.php';
use \App\Controllers\MainController;
$controller = new MainController();
$current_user = $controller->get_current_user();

$code = isset($_REQUEST['code']) ? $_REQUEST['code'] : 0;
$page = isset($_REQUEST['page']) ? $_REQUEST['page'] : 1;
$id = isset($_REQUEST['id']) ? (int)$_REQUEST['id'] : 0;
$name = isset($_REQUEST['name']) ? $_REQUEST['name'] : '';
$email = isset($_REQUEST['email']) ? $_REQUEST['email'] : '';
$phone = isset($_REQUEST['phone']) ? $_REQUEST['phone'] : '';
$address = isset($_REQUEST['address']) ? $_REQUEST['address'] : '';
$password_hash = isset($_REQUEST['password_hash']) ? $_REQUEST['password_hash'] : '';
$role_id = isset($_REQUEST['role_id']) ? $_REQUEST['role_id'] : 0;
$item = isset($_REQUEST['id']) ? $controller->read_user($_REQUEST['id']) : null;
$page_items = 5;
$total_items = $controller->count_users();        
$total_pages = ceil($total_items/$page_items);
$tdata_columns = array(
    'Id'=>'id',
    'Nombre'=>'name',
    'Correo'=>'email',
    'Teléfono'=>'phone',
    'Directión'=>'address',
    'Rol'=> 'role_name'
    );
$tdata_items = $controller->read_all_users($page_items, $page_items*($page-1));  
$roles_list = $controller->get_roles_list();

if ( isset($_REQUEST['action']) ) {
    
    // Action create.
    if ( $_REQUEST['action']==='create' ) {
        $controller->create_user($name, $email, $phone, $address, $role_id);

    // Action update.
    } else if ( $_REQUEST['action']==='update' ) {
        $controller->update_user($id, $name, $email, $phone, $address, 
                                 $password_hash, $role_id);
        
    
    // Action delete.
    } else if ( $_REQUEST['action']==='delete' ) {
        $controller->delete_user($id);
    
    // Action details.
    } else if ( $_REQUEST['action']==='details' ) {
        $controller->details_user($id);
    
    // Action discard.
    } else if ( $_REQUEST['action']==='discard' ) {
        $controller->discard();
    }
    
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?php echo SITE_NAME ?></title>
        <!-- Custom CSS -->
        <link rel="stylesheet" href="/css/styles.css"/>
        <link rel="stylesheet" href="/css/tdata.css"/>
        <link rel="stylesheet" href="/css/page_controls.css"/>
    </head>
    <body>
        <div class="container">
            <?php include HEADER ?>
            <?php include NAV ?>            
            <main>
                <?php include NAV_ADMIN ?>
                <?php if ( $code ) { ?>
                <div class="box-message">
                    <?php echo T_MESSAGE[$code] ?>
                </div>
                <?php } ?>
                <div class="box-main">
                    <h2>Gestionar usuarios</h2>
                    <form method="POST" action="<?php echo_php_self() ?>" >       
                        <div class="tdata-box">
                            <h3>Listado de usuarios</h3>
                            <?php include TDATA ?>
                        </div>
                        <div>
                            <button type="submit" name="action" value="delete">Borrar</button>
                            <button type="submit" name="action" value="details">Ver detalles</button>
                        </div>
                    </form>
                    <div class="page-controls-box">    
                        <?php include PAGE_CONTROLS ?>
                    </div>
                    <div>
                        <form method="POST" action="<?php echo_php_self() ?>" >
                            <h3>Detalles del usuario <?php echo isset($item) ? (string)$item->id : '' ?> </h3>
                            <div class="box-details">
                                <label>Nombre:</label>
                                <input type="text" name="name" 
                                    value="<?php echo isset($item) ? $item->name : '' ?>" 
                                    placeholder="Nombre del usuario"/>
                                <label>Correo:</label>
                                <input type="text" name="email" 
                                    value="<?php echo isset($item) ? $item->email : '' ?>" 
                                    placeholder="Correo del usuario"/>
                                <label>Teléfono:</label>
                                <input type="text" name="phone" 
                                    value="<?php echo isset($item) ? $item->phone : '' ?>" 
                                    placeholder="Teléfono del usuario"/>
                                <label>Dirección:</label>
                                <input type="text" name="address" 
                                    value="<?php echo isset($item) ? $item->address : '' ?>" 
                                    placeholder="Dirección del usuario"/>
                                <label>Rol:</label>
                                <select name="role_id">
                                    <option disabled="disabled" selected>-- Rol del usuario --</option>
                                    <?php 
                                        foreach ($roles_list as $role_item) {
                                            echo '<option value="' . $role_item[0] .'"';
                                            if ( isset($item) && $item->role->id==$role_item[0]) {
                                                echo ' selected="selected"';                            
                                            }
                                            echo '>';
                                            echo $role_item[1];
                                            echo '</option>';
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="box-actions">
                                <input type="hidden" name="id" value="<?php echo isset($item) ? $item->id : 0 ?>"/>
                                <input type="hidden" name="password_hash" value="<?php echo isset($item) ? $item->password_hash : '' ?>"/>
                                <button type="submit" name="action" 
                                        <?php echo isset($item) ? 'disabled="disabled"' : '' ?> 
                                        value="create">Crear</button>
                                <button type="submit" name="action" 
                                        <?php echo !isset($item) ? 'disabled="disabled"' : '' ?> 
                                        value="update">Modificar</button>
                                <button type="submit" name="action" 
                                        <?php echo !isset($item) ? 'disabled="disabled"' : '' ?> 
                                        value="discard">Descartar cambios</button>
                            </div>
                        </form>
                    </div>
                </div>
            </main>
            <?php include FOOTER ?>
        </div>
    </body>    
</html>
