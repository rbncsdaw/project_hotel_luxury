<?php
/**
 * Administration zone: role management page.
 * Project Hotel Luxury.
 * Rubén Rocha, Andrea Muñoz.
 * 2021
 */
require_once '../../global.php';
use \App\Controllers\MainController;
$controller = new MainController();
$current_user = $controller->get_current_user();

$code = isset($_REQUEST['code']) ? $_REQUEST['code'] : 0;
$page = isset($_REQUEST['page']) ? $_REQUEST['page'] : 1;
$item = isset($_REQUEST['id']) ? $controller->read_role($_REQUEST['id']) : null;
$name = isset($_REQUEST['name']) ? $_REQUEST['name'] : '';
$id = isset($_REQUEST['id']) ? $_REQUEST['id'] : 0;
$page_items = 5;
$total_items = $controller->count_roles();        
$total_pages = ceil($total_items/$page_items);
$tdata_columns = array(
    'Id'=>'id',
    'Nombre'=>'name');
$tdata_items = $controller->read_all_roles($page_items, $page_items*($page-1));  

if ( isset($_REQUEST['action']) ) {
    
    // Action create.
    if ( $_REQUEST['action']==='create' ) {
        $controller->create_role($name);

    // Action update.
    } else if ( $_REQUEST['action']==='update' ) {
        $controller->update_role($id, $name);
    
    // Action delete.
    } else if ( $_REQUEST['action']==='delete' ) {
        $controller->delete_role($id);
    
    // Action details.
    } else if ( $_REQUEST['action']==='details' ) {
        $controller->details_role($id);
    
    // Action discard.
    } else if ( $_REQUEST['action']==='discard' ) {
        $controller->discard();
    }

}



?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?php echo SITE_NAME ?></title>
        <!-- Custom CSS -->
        <link rel="stylesheet" href="/css/styles.css"/>
        <link rel="stylesheet" href="/css/tdata.css"/>
        <link rel="stylesheet" href="/css/page_controls.css"/>
    </head>
    <body>
        <div class="container">
            <?php include HEADER ?>
            <?php include NAV ?>            
            <main>
                <?php include NAV_ADMIN ?>
                <?php if ( $code ) { ?>
                <div class="box-message">
                    <?php echo T_MESSAGE[$code] ?>
                </div>
                <?php } ?>
                <div class="box-main">
                    <h2>Gestionar roles</h2>
                    <form method="POST" action="<?php echo_php_self() ?>" >       
                        <div class="tdata-box">
                            <h3>Listado de roles</h3>
                            <?php include TDATA ?>
                        </div>
                        <div>
                            <button type="submit" name="action" value="delete">Borrar</button>
                            <button type="submit" name="action" value="details">Ver detalles</button>
                        </div>
                    </form>
                    <div class="page-controls-box">    
                        <?php include PAGE_CONTROLS ?>
                    </div>
                    <div>
                        <form method="POST" action="<?php echo_php_self() ?>" >
                            <h3>Detalles del rol <?php echo isset($item) ? (string)$item->id : '' ?> </h3>
                            <div class="box-details">
                                <label>Nombre:</label>
                                <input type="text" name="name" 
                                    value="<?php echo isset($item) ? $item->name : '' ?>" 
                                    placeholder="Nombre del rol"/>
                            </div>
                            <div class="box-actions">
                                <input type="hidden" name="id" value="<?php echo isset($item) ? $item->id : 0 ?>"/>
                                <button type="submit" name="action" 
                                        <?php echo isset($item) ? 'disabled="disabled"' : '' ?> 
                                        value="create">Crear</button>
                                <button type="submit" name="action" 
                                        <?php echo !isset($item) ? 'disabled="disabled"' : '' ?> 
                                        value="update">Modificar</button>
                                <button type="submit" name="action" 
                                        <?php echo !isset($item) ? 'disabled="disabled"' : '' ?> 
                                        value="discard">Descartar cambios</button>
                            </div>
                        </form>
                    </div>
                </div>
            </main>
            <?php include FOOTER ?>
        </div>
    </body>    
</html>
