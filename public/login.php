<?php
/**
 * Login page for project.
 * Project Hotel Luxury.
 * Rubén Rocha, Andrea Muñoz.
 * 2021
 */
require_once '../global.php';
use \App\Controllers\MainController;

$controller = new MainController();
$current_user = $controller->get_current_user();

$code = isset($_REQUEST['code']) ? (int)$_REQUEST['code'] : 0;
$username = isset($_REQUEST['username']) ? $_REQUEST['username'] : USER_EMAIL_ADMIN;
$password = isset($_REQUEST['password']) ? $_REQUEST['password'] : DEFAULT_PASSWORD;

// Action login.
if ( isset($_REQUEST['action']) && $_REQUEST['action']==='login') {
    $controller->login($username, $password);
}

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?php echo SITE_NAME ?></title>
        <!-- Custom CSS -->
        <link rel="stylesheet" href="/css/styles.css"/>
        <link rel="stylesheet" href="/css/tdata.css"/>
        <link rel="stylesheet" href="/css/page_controls.css"/>
    </head>
    <body>
        <div class="container">
            <?php include HEADER ?>
            <?php include NAV ?>
            <main>
                <?php if ( $code ) { ?>
                <div class="box-message">
                    <?php echo T_MESSAGE[$code] ?>
                </div>
                <?php } ?>
                <div class="box-main">
                    <form class="form-main" action="<?php echo php_self() ?>" method="POST">
                        <h2>Login</h2>
                        <label for="username">Usuario</label>
                        <input type="email" name="username" 
                               value="<?php echo $username ?>" 
                               placeholder="email@luxury.example"/>
                        <label for="password">Contraseña</label>
                        <input type="password" name="password" 
                               value="<?php echo $password ?>"/>
                        <button type="submit" name="action" value="login">Login</button>                        
                    </form>
                </div>
            </main>
            <?php include FOOTER ?>
        </div>
    </body>    
</html>
