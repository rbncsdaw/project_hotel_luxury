<?php
/**
 * Home page for project.
 * Project Hotel Luxury.
 * Rubén Rocha, Andrea Muñoz.
 * 2021
 */
require_once '../global.php';
use \App\Controllers\MainController;

$controller = new MainController();
$current_user = $controller->get_current_user();

$code = isset($_REQUEST['code']) ? (int)$_REQUEST['code'] : 0;
$date_from = isset($_REQUEST['date_from']) ? $_REQUEST['date_from'] : '2021-03-18';
$date_to = isset($_REQUEST['date_to']) ? $_REQUEST['date_to'] : '2021-03-19';

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?php echo SITE_NAME ?></title>
        <!-- Custom CSS -->
        <link rel="stylesheet" href="/css/styles.css"/>
        <link rel="stylesheet" href="/css/tdata.css"/>
        <link rel="stylesheet" href="/css/page_controls.css"/>
    </head>
    <body>
        <div class="container">
            <?php include HEADER ?>
            <?php include NAV ?>
            <main>
                <?php if ( $code ) { ?>
                <div class="box-message">
                    <?php echo T_MESSAGE[$code] ?>
                </div>
                <?php } ?>
                <div class="box-main">
                    <form class="form-main" action="<?php echo php_self() ?>" method="POST">
                        <h2>Buscar habitación</h2>
                        <label for="date-from">Desde</label>
                        <input type="date" name="date-from" value="<?php echo $date_from ?>"/>
                        <label for="date-to">Hasta</label>
                        <input type="date" name="date-from" value="<?php echo $date_to ?>"/>                        
                        <button type="submit" name="action" value="search">Buscar</button>
                    </form>
                    <div>
                        <a href="/room.php">RESULTADOS_BUSQUEDA</a>
                    </div>
                </div>
            </main>
            <?php include FOOTER ?>
        </div>
    </body>    
</html>
