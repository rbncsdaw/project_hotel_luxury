<?php
/**
 * Logout page for project.
 * Project Hotel Luxury.
 * Rubén Rocha, Andrea Muñoz.
 * 2021
 */
require_once '../global.php';
use \App\Controllers\MainController;
$controller = new MainController();
$controller->logout();
?>