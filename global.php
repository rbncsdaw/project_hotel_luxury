<?php
/**
 * Global configuration file.
 * Project Hotel Luxury.
 * Rubén Rocha, Andrea Muñoz.
 * 2021
 */
use App\Models\Role;

define('DS', DIRECTORY_SEPARATOR);
define('ROOT', __DIR__ . DS);

 /**
 * Requires.
 */
require_once(ROOT . 'exception_handler.php');
require_once(ROOT . 'autoload.php');
require_once(ROOT . 'helpers.php');

/**
 * File and directorie paths.
 */
// Configuration files (database, app, etc).
define('CONFIG_DB_XML', ROOT . 'config' . DS . 'database' . DS . 'database.xml');
define('CONFIG_DB_XSD', ROOT . 'config' . DS . 'database' . DS . 'database.xsd');
define('CONFIG_APP_XML', ROOT . 'config' . DS . 'app' . DS . 'app.xml');
define('CONFIG_APP_XSD', ROOT . 'config' . DS . 'app' . DS . 'app.xsd');
// Directories.
define('MODELS', ROOT . 'App' . DS . 'Models' . DS);
define('CONTROLLERS', ROOT . 'App' . DS . 'Controllers' . DS);
define('COMPONENTS', ROOT . 'components' . DS);
define('TEMPLATES', ROOT . 'templates' . DS);
define('TEMPLATES_ADMIN', TEMPLATES . 'admin' . DS);
// Templates.
define('HEADER', TEMPLATES . 'header.php');
define('NAV', TEMPLATES . 'nav.php');
define('FOOTER', TEMPLATES . 'footer.php');
define('ERROR', TEMPLATES . 'error.php');
define('TDATA', TEMPLATES . 'tdata.php');
define('PAGE_CONTROLS', TEMPLATES . 'page_controls.php');
define('NAV_ADMIN', TEMPLATES_ADMIN . 'nav.php');

/**
 * Verify that the directories and files exist.
 */
if ( !is_dir(ROOT) ) throw new \Exception('Directorio no encontrado: '. ROOT);
 if ( !is_dir(MODELS) ) throw new \Exception('Directorio no encontrado: '. MODELS);
if ( !is_dir(CONTROLLERS) ) throw new \Exception('Directorio no encontrado: '. CONTROLLERS);
if ( !is_dir(TEMPLATES) ) throw new \Exception('Directorio no encontrado: '. TEMPLATES);
if ( !is_dir(TEMPLATES_ADMIN) ) throw new \Exception('Directorio no encontrado: '. TEMPLATES_ADMIN);
 if ( !file_exists(CONFIG_DB_XML) ) throw new \Exception('Fichero no encontrado: '. CONFIG_DB_XML);
if ( !file_exists(CONFIG_DB_XSD) ) throw new \Exception('Fichero no encontrado: '. CONFIG_DB_XSD);
if ( !file_exists(CONFIG_APP_XML) ) throw new \Exception('Fichero no encontrado: '. CONFIG_APP_XML);
if ( !file_exists(CONFIG_APP_XSD) ) throw new \Exception('Fichero no encontrado: '. CONFIG_APP_XSD);
if ( !file_exists(HEADER) ) throw new \Exception('Fichero no encontrado: '. HEADER);
if ( !file_exists(NAV) ) throw new \Exception('Fichero no encontrado: '. NAV);
if ( !file_exists(FOOTER) ) throw new \Exception('Fichero no encontrado: '. FOOTER);
if ( !file_exists(ERROR) ) throw new \Exception('Fichero no encontrado: '. ERROR);
if ( !file_exists(TDATA) ) throw new \Exception('Fichero no encontrado: '. TDATA);
if ( !file_exists(PAGE_CONTROLS) ) throw new \Exception('Fichero no encontrado: '. PAGE_CONTROLS);
if ( !file_exists(NAV_ADMIN) ) throw new \Exception('Fichero no encontrado: '. NAV_ADMIN);

/**
 * Application configuration (load from configuration files).
 */
$data = load_config_app();
define('SITE_NAME', $data['site_name']);
unset($data);

/**
 * 
 */
define('RESTRICTED_PAGES', array(
    '/logout.php' => array(Role::ROLE_ADMIN,
                           Role::ROLE_REGISTER),
    '/login.php' => array(Role::ROLE_ANONIMOUS),
    '/register.php' => array(Role::ROLE_ANONIMOUS)
));

/**
 * Codes/messages translator.
 */
define('T_MESSAGE', array(
    '0' => '',
    '1' => 'Usuario no válido.',
    '2' => 'Introduce el usuario.',
    '3' => 'Introduce la contraseña.',
    '4' => 'Necesario iniciar sesión para acceder.',
    '5' => 'Faltan datos obligatorios.',
    '6' => 'Las contraseñas no coinciden.',
    '7' => 'Hasta pronto!',
    '8' => 'Bienvenido al hotel!',
    '9' => 'Creado correctamente!',
    '10' => 'Eliminado correctamente!',
    '11' => 'Modificado correctamente!',
    '100' => 'No tienes permiso para acceder a la página!',
    '110' => 'Usuario registrado correctamente!',
    '120' => 'Ya existe un usuario con esos datos!'
)
);

/**
 * Others.
 */
define('DEFAULT_PASSWORD', 'abc123.');
define('USER_EMAIL_ADMIN', 'admin@luxury.example');
define('USER_EMAIL_ANONYMOUS', 'anonymous@luxury.example');
 


?>