<?php
/**
 * Helpers.
 * Project Hotel Luxury.
 * Rubén Rocha, Andrea Muñoz.
 * 2021
 */


/**
 * Return the current url.
 *
 * @return string Url of the current pasg.
 */
function php_self() {
    return htmlspecialchars($_SERVER['PHP_SELF']);
}

/**
 * Print the current url.
 *
 * @return void
 */
function echo_php_self() {
    echo php_self();
}

/**
 * Load application configuration.
 *
 * @return void
 */
function load_config_app() {
    // Read file XML and create DOM.
    $root = new \DOMDocument();
    $root->load(CONFIG_APP_XML);
    $is_valid = $root->schemaValidate(CONFIG_APP_XSD);
    // Load data from file.
    if ( $is_valid ) {
        $xml = simplexml_load_file(CONFIG_APP_XML);
        $data = array();
        $data['site_name'] = (string)$xml->xpath("//site_name")[0];

    } else {
        throw new \Exception("Fichero de configuración no válido.");
    }
    return $data;
}

/**
 * Print a variable and die.
 */
function debug_show_var($var){
    echo '<pre>';
    print_r($var);
    die();
}

?>