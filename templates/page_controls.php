<!-- Start page_controls -->
<?php
function url_page($i) {
    $url = $_SERVER['PHP_SELF'] . '?page=' . $i;
    return $url;
}
function url_first_page() {
    $url = $_SERVER['PHP_SELF'] . '?page=1';
    return $url;
}
function url_last_page($total_pages) {
    $url = $_SERVER['PHP_SELF'] . '?page=' . $total_pages;
    return $url;
}
function url_next_page($page, $total_pages) {
    if ($page == $total_pages) {
        $url = '';
    } else {
        $url = $_SERVER['PHP_SELF'] . '?page=' . min(($page+1), $total_pages);
    }
    return $url;
}
function url_previous_page($page) {
    if ($page == 1) {
        $url = '';
    } else {
        $url = $_SERVER['PHP_SELF'] . '?page=' . max(($page-1), 1);
    }
    return $url;
}
?>
<div class="page-controls">
    <ul>
        <li>
            <button onclick="location.href='<?php echo url_first_page() ?>'">
                |&lt;
            </button>
        </li>
        <li>
            <button onclick="location.href='<?php echo url_previous_page($page) ?>'">
                &lt;&lt;
            </button>
        </li>                            
        <li>
            <label><?php echo $page . ' de ' . $total_pages ?></label>
        </li>
        <li>
            <button onclick="location.href='<?php echo url_next_page($page, $total_pages) ?>'">
                &gt;&gt;
            </button>
        </li>
        <li>
            <button onclick="location.href='<?php echo url_last_page($total_pages) ?>'">
                &gt;|
            </button>
        </li>
    </ul>                    
</div> 
<!-- End page_controls -->