<nav>
    <ul>
        <li>Home</li>
        <li>Habitaciones</li>        
        <!-- Only anonymous user -->
        <?php if( $current_user->is_anonymous() ) { ?>
        <li><a href="/register.php">register</a></li>
        <li><a href="/login.php">login</a></li>
        <?php } ?>
        <!-- Only register user + admin user -->
        <?php if( $current_user->is_register() ||
                  $current_user->is_admin() ) { ?>
        <li><a href="/profile.php">perfil</a></li>
        <li><a href="/logout.php">logout</a></li>
        <?php } ?>
        <!-- Only admin user -->
        <?php if( $current_user->is_admin() ) { ?>
        <li><a href="/admin/index.php">Administracion</a></li>
        <?php } ?>
    </ul>
    <div class="login-box">
        <span>
            <?php echo $_SESSION['username'] ?>
        </span>
    </div>    
</nav>