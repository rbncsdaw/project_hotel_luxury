<h2>Gestionar usuarios</h2>
<form method="POST" action="<?php echo_php_self() ?>" >       
    <div class="tdata-box">
        <h3>Listado de usuarios</h3>
        <?php include TDATA ?>
    </div>
    <div>
        <button type="submit" name="action" value="delete">Borrar</button>
        <button type="submit" name="action" value="details">Ver detalles</button>
    </div>
</form>
<div class="page-controls-box">    
    <?php include PAGE_CONTROLS ?>
</div>
<div>
    <form method="POST" action="<?php echo_php_self() ?>" >
        <h3>Detalles del usuario <?php echo isset($item) ? (string)$item->id : '' ?> </h3>
        <div class="box-details">
            <label>Nombre:</label>
            <input type="text" name="name" 
                   value="<?php echo isset($item) ? $item->name : '' ?>" 
                   placeholder="Nombre del usuario"/>
            <label>Correo:</label>
            <input type="text" name="email" 
                   value="<?php echo isset($item) ? $item->email : '' ?>" 
                   placeholder="Correo del usuario"/>
            <label>Teléfono:</label>
            <input type="text" name="phone" 
                   value="<?php echo isset($item) ? $item->phone : '' ?>" 
                   placeholder="Teléfono del usuario"/>
            <label>Dirección:</label>
            <input type="text" name="address" 
                   value="<?php echo isset($item) ? $item->address : '' ?>" 
                   placeholder="Dirección del usuario"/>
            <label>Rol:</label>
            <select name="role_id">
                <option disabled="disabled" selected>-- Rol del usuario --</option>
                <?php 
                    foreach ($roles_list as $role_item) {
                        echo '<option value="' . $role_item[0] .'"';
                        if ( isset($item) && $item->role->id==$role_item[0]) {
                            echo ' selected="selected"';                            
                        }
                        echo '>';
                        echo $role_item[1];
                        echo '</option>';
                    }
                ?>
            </select>
        </div>
        <div class="box-actions">
            <input type="hidden" name="id" value="<?php echo isset($item) ? $item->id : 0 ?>"/>
            <button type="submit" name="action" 
                    <?php echo isset($item) ? 'disabled="disabled"' : '' ?> 
                    value="create">Crear</button>
            <button type="submit" name="action" 
                    <?php echo !isset($item) ? 'disabled="disabled"' : '' ?> 
                    value="update">Modificar</button>
            <button type="submit" name="action" 
                    <?php echo !isset($item) ? 'disabled="disabled"' : '' ?> 
                    value="discard">Descartar cambios</button>
        </div>
    </form>
</div>
