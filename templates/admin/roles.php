<h2>Gestionar roles</h2>
<form method="POST" action="<?php echo_php_self() ?>" >       
    <div class="tdata-box">
        <h3>Listado de roles</h3>
        <?php include TDATA ?>
    </div>
    <div>
        <button type="submit" name="action" value="delete">Borrar</button>
        <button type="submit" name="action" value="details">Ver detalles</button>
    </div>
</form>
<div class="page-controls-box">    
    <?php include PAGE_CONTROLS ?>
</div>
<div>
    <form method="POST" action="<?php echo_php_self() ?>" >
        <h3>Detalles del rol <?php echo isset($item) ? (string)$item->id : '' ?> </h3>
        <div class="box-details">
            <label>Nombre:</label>
            <input type="text" name="name" 
                   value="<?php echo isset($item) ? $item->name : '' ?>" 
                   placeholder="Nombre del rol"/>
        </div>
        <div class="box-actions">
            <input type="hidden" name="id" value="<?php echo isset($item) ? $item->id : 0 ?>"/>
            <button type="submit" name="action" 
                    <?php echo isset($item) ? 'disabled="disabled"' : '' ?> 
                    value="create">Crear</button>
            <button type="submit" name="action" 
                    <?php echo !isset($item) ? 'disabled="disabled"' : '' ?> 
                    value="update">Modificar</button>
            <button type="submit" name="action" 
                    <?php echo !isset($item) ? 'disabled="disabled"' : '' ?> 
                    value="discard">Descartar cambios</button>
        </div>
    </form>
</div>
