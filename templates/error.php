<div class="box-error">
    <h2>Error</h2>
    <table>
        <tr>
            <td>Fichero:</td>
            <td><?php echo $e->getFile() ?></td>
        </tr>
        <tr>
            <td>Línea:</td>
            <td><?php echo $e->getLine() ?></td>
        </tr>
        <tr>
            <td>Mensaje:</td>
            <td><?php echo $e->getMessage() ?></td>
        </tr>
        <tr>
            <td>Trazas:</td>
            <td><p><?php echo preg_replace('/\n/', '<br/>', $e->getTraceAsString()); ?></p></td>
        </tr>
    </table>                    
</div>