<!-- Start tdata -->
<?php    
    $tdata_headers = array_keys($tdata_columns);
    $tdata_ids = array_values($tdata_columns);
    $tdata_num_columns = count($tdata_headers) + 2;    
    function css_odd_even($index) {
        return ($index%2 ? 'tdata-odd-row': 'tdata-even-row');
    }   
?>
<style>
    .tdata-container {
        grid-template-columns: repeat(<?php echo $tdata_num_columns-1 ?>, min-content) auto;
        font-size: 0.9rem;
    }
</style>
<div class="tdata-container"> 
    <!-- Table head -->
    <div class="tdata-head-cell"></div>
    <?php foreach ($tdata_headers as $value) {?>
    <div class="tdata-head-cell"><?php echo $value ?></div>
    <?php } ?>
    <div class="tdata-head-cell"></div>
    <!-- Table body -->                
    <?php foreach ($tdata_items as $index=>$tdata_item) { ?>
    <div class="<?php echo css_odd_even($index) ?> tdata-body-cell">
        <input type="radio" name="id" value="<?php echo $tdata_item->id ?>"/>
    </div>
    <?php foreach ($tdata_ids as $value) { ?>        
    <div class="<?php echo css_odd_even($index) ?> tdata-body-cell">
        <?php echo $tdata_item->$value ?>
    </div>
    <?php } ?>
    <div class="<?php echo css_odd_even($index) ?> tdata-body-cell"></div>
    <?php } ?>
</div>
<!-- End tdata -->